<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function welcome()
    {
        $newCommers = Profile::where('isVerified',1)->paginate(8);
        return view('welcome')->with('newCommers',$newCommers);
    }

    public function about()
    {
        return view('about');
    }

    public function privacy()
    {
        return view('privacy');
    }
}
