<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use App\Profession;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $professions = Profession::orderBy('title','asc')->get();
        $alumnis = User::where('completeProfile',1)->where('isVerified',1)->paginate(24);
        return view('home')->with('alumnis',$alumnis)->with('professions',$professions);
    }

    public function search(Request $req)
    {
        $isName = 0;
        $isLocation = 0;
        $isProfession = 0;
        $name = null;
        $location = null;
        $profession = null;
        $professions = Profession::orderBy('title','asc')->get();
        if(isset($req->name))
        {
            $isName = 1;
            $name = User::where('name','like','%'.$req->name.'%')->where('isVerified',1)->paginate(4);
        } if(isset($req->location))
        {
            $isLocation = 1;
            $location = Profile::where('living','like','%'.$req->location.'%')->where('isVerified',1)->paginate(4);
        } if(isset($req->profession_id))
        {
            $isProfession = 1;
            $byProfession = Profile::where('profession_id',$req->profession_id)->where('isVerified',1)->paginate(4);
        }

        return view('search',compact('isName','isLocation','isProfession','name','location','byProfession','professions'));

    }
}
