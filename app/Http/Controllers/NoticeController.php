<?php

namespace App\Http\Controllers;

use File;
use Auth;
use Session;
use App\Notice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::paginate(6);
        return view('notice.index')->with('notices',$notices);
    }

    public function list()
    {
        $notices = Notice::all();
        return view('notice.list')->with('notices',$notices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $notice = new Notice;
        if($req->hasFile('image'))
        {
            $image = $req->image;
            $image_new_name = time().$image->getClientOriginalName();
            $image->move('uploads/notice', $image_new_name);
            $notice->image = 'uploads/notice/'.$image_new_name;
        }
        $notice->title = $req->title;
        $notice->body = $req->body;
        $notice->user_id = Auth::user()->id;
        $notice->save();

        Session::flash('success','Notice Created Successfully');
        return redirect()->route('notice.list');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function show(Notice $notice)
    {
        return view('notice.single')->with('notice',$notice);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit(Notice $notice)
    {
        return view('notice.edit')->with('notice',$notice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Notice $notice)
    {
        if($req->hasFile('image'))
        {
            $image_path = $notice->image;
            if (File::exists($image_path)) {
               unlink($image_path);
           }
            $image = $req->image;
            $image_new_name = time().$image->getClientOriginalName();
            $image->move('uploads/notice', $image_new_name);
            $notice->image = 'uploads/notice/'.$image_new_name;
        }
        $notice->title = $req->title;
        $notice->body = $req->body;
        $notice->save();
        Session::flash('success','Notice Updated Successfully');
        return redirect()->route('notice.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notice $notice)
    {
        $notice->delete();

        Session::flash('success','Notice Deleted Successfully');
        return redirect()->route('notice.list');
    }
}
