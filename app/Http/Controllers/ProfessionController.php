<?php

namespace App\Http\Controllers;

use Session;
use App\Profession;
use Illuminate\Http\Request;

class ProfessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profession.index')->with('professions',Profession::orderBy('title','asc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profession.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $profession = new Profession;
        $profession->title=$req->title;
        if($profession->save())
        {
            Session::flash('success','Profession Added Successfully');
        } else{
            Session::flash('error','Something Went Wrong');
        }

        return redirect()->route('profession.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function show(Profession $profession)
    {
        return view('profession',compact('profession'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function edit(Profession $profession)
    {
        return view('profession.edit')->with('profession',$profession);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Profession $profession)
    {
        $profession->title=$req->title;
        if($profession->save())
        {
            Session::flash('success','Profession Updated Successfully');
        } else{
            Session::flash('error','Something Went Wrong');
        }

        return redirect()->route('profession.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profession $profession)
    {
        $profession->delete();
        Session::flash('success','Profession Deleted Successfully');
        return redirect()->back();
    }
}
