<?php

namespace App\Http\Controllers;

use File;
use Auth;
use Session;
use App\User;
use App\Profile;
use App\Profession;
use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.index')->with('profile',Auth::user()->profile);
    }

    public function list()
    {
        $users = User::where('completeProfile',1)->get();
        return view('user.list')->with('users',$users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->completeProfile)
        {
            return redirect()->route('profile.edit');
        }
        $professions = Profession::orderBy('title','asc')->get();
        return view('profile.create')->with('professions',$professions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $req)
    {
        $user = Auth::user();
        $prof = new Profile;
        if($req->hasFile('avatar'))
        {
            $avatar = $req->avatar;
            $avatar_new_name = time().$avatar->getClientOriginalName();
            $avatar->move('uploads/profile', $avatar_new_name);
            $prof->avatar = 'uploads/profile/'.$avatar_new_name;
        }
        $prof->user_id = $user->id;
        $prof->dob = $req->dob;
        $prof->ssc_batch = $req->ssc_batch;
        $prof->profession_id = $req->profession_id;
        $prof->company = $req->company;
        $prof->designation = $req->designation;
        $prof->living = $req->living;
        $prof->phone = $req->phone;
        $prof->facebook_url = $req->facebook_url;
        $prof->instagram_url = $req->instagram_url;
        $prof->twitter_url = $req->twitter_url;
        $prof->short_desc = $req->short_desc;
        if($prof->save())
        {
            $findUser = User::where('id',$user->id)->first();
            $findUser->completeProfile = 1;
            $findUser->save();
            Session::flash('success','Profile Created Successfully');
        }
        else{
            Session::flash('error','Something Went Wrong');
        }

        return redirect()->route('profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        return view('profile.index')->with('profile',$profile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $professions = Profession::orderBy('title','asc')->get();
        $profile = Profile::where('user_id',Auth::user()->id)->first();
        return view('profile.edit')->with('profile',$profile)->with('professions',$professions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $prof = Profile::where('user_id',Auth::user()->id)->first();
        if($req->hasFile('avatar'))
        {
            $avatar_path = $prof->avatar;
            if (File::exists($avatar_path)) {
               unlink($avatar_path);
           }
            $avatar = $req->avatar;
            $avatar_new_name = time().$avatar->getClientOriginalName();
            $avatar->move('uploads/profile', $avatar_new_name);
            $prof->avatar = 'uploads/profile/'.$avatar_new_name;
        }
        $prof->dob = $req->dob;
        $prof->ssc_batch = $req->ssc_batch;
        $prof->profession_id = $req->profession_id;
        $prof->company = $req->company;
        $prof->designation = $req->designation;
        $prof->living = $req->living;
        $prof->phone = $req->phone;
        $prof->facebook_url = $req->facebook_url;
        $prof->instagram_url = $req->instagram_url;
        $prof->twitter_url = $req->twitter_url;
        $prof->short_desc = $req->short_desc;
        if($prof->save())
        {
            Session::flash('success','Profile Updated Successfully');
        } else{
            Session::flash('error','Something Went Wrong');
        }

        return redirect()->route('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }

    public function approve($id)
    {
        $prof = Profile::findOrFail($id);
        $user = User::findOrFail($prof->user_id);
        $user->isVerified = 1;
        $prof->isVerified = 1; 
        $user->save();
        $prof->save();
        Session::flash('success','User has been verified');
        return redirect()->back();
    }

    public function makeAdmin($id)
    {
        $prof = Profile::findOrFail($id);
        $user = User::findOrFail($prof->user_id);
        $user->isAdmin = 1;
        $user->save();

        Session::flash('success','User has been grant as Admin');
        return redirect()->back();
    }

    public function removeAdmin($id)
    {
        $prof = Profile::findOrFail($id);
        $user = User::findOrFail($prof->user_id);
        if($user->id == Auth::user()->id)
        {
            Session::flash('error','Sorry You Can Not Remove You From Admin');
            return redirect()->back();
        }
        $user->isAdmin = 0;
        $user->save();

        Session::flash('success','User has been demoted by Admin');
        return redirect()->back();
    }
}
