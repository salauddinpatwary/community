<?php

namespace App\Http\Middleware;

use Auth;
use Session;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            if(Auth::user()->isAdmin)
            {
                return $next($request);
            }
            Session::flash('info','What Are You Looking For?');
            return redirect()->route('home');
        }
        Session::flash('info','Please Login To Continue');

        return redirect()->route('welcome');
        
    }
}
