<?php

namespace App\Http\Middleware;

use Auth;
use Session;
use Closure;

class Verified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            if(Auth::user()->isVerified || Auth::user()->isAdmin) //Admin also can see anything with his third eye. :P
            {
                return $next($request);
            }
            Session::flash('info','Please Wait Until Your Profile Gets Verified');
            return redirect()->route('profile');
        }
        Session::flash('info','Please Login To Continue');

        return redirect()->route('welcome');
        
    }
}
