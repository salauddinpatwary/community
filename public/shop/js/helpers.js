$(document).ready(function(){

flyer_load();
cart_upper_load();
cart_item_load();

function cart_item_load()
{
  $('peekout_cart_item_load').html('');
  $.ajax({
    type: 'get',
    url: '/cart/item/load',
    success:function(data){
        if(data.cart !== 0)
        {
          var i = 0;
          $('.peekout_cart_item_load').html('');
          for(var prop in data.cart)
          {
            $('.peekout_cart_item_load').append('<tr><td><i id="increase_item" pid="'+data.cart[prop].rowId+'" qty="'+data.cart[prop].qty+'" class="fa fa-plus-square peekout_cart_icon_zoom" style="color:#0080ff; cursor:pointer;"></i></td><td id="item-quantity">'+data.cart[prop].qty+'</td><td><i id="decrease_item" pid="'+data.cart[prop].rowId+'" qty="'+data.cart[prop].qty+'" class="fa fa-minus-square peekout_cart_icon_zoom" style="color:#0080ff; cursor:pointer;"></i></td><td>'+data.cart[prop].name+'</td><td><img src="/'+data.img[i]+'" style="height:25px; width:20px;"></td><td>'+data.cart[prop].price * data.cart[prop].qty+'</td><td><i id="trash_item" rID="'+data.cart[prop].rowId+'" class="fa fa-trash peekout_cart_icon_zoom" style="color:#ff0040; cursor:pointer;"></i></td></tr>');
            i++;
          }
        }
      else {
        $('.peekout_cart_item_load').html('<tr><td><b>Cart is Empty.</b></td></tr>');
      }
    }
  })
}

function flyer_load()
{
  $.ajax({
    type:'get',
    url:'/flyer/load/cart',
    success:function(data){
      $('.itemCount').html(data.item);
      $('.cart_total_taka').html(data.amount);
      $('.odometer').html(data.amount);
    }
  })
}

function cart_upper_load()
{
  $.ajax({
    type:'get',
    url:'/cart/upper/load',
    success:function(data){
      $('#cart_item').html(data.item);
      $('#amount').html(data.amount);
      $('#delivery').html(data.delivery);
      $('#grand_total').html(data.total);
    }
  })
}

$('body').delegate('#increase_item','click',function(event){
  event.preventDefault();
  var id = $(this).attr('pid');
  var qnty = $(this).attr('qty');
  $.ajax({
    type:'get',
    url:'/increase/cart/item/'+id+'/'+qnty,
  })

  flyer_load();
  cart_upper_load();
  cart_item_load();

});

$('body').delegate('#decrease_item','click',function(event){
  event.preventDefault();
  var id = $(this).attr('pid');
  var qnty = $(this).attr('qty');
  $.ajax({
    type:'get',
    url:'/decrease/cart/item/'+id+'/'+qnty,
  })
  flyer_load();
  cart_upper_load();
  cart_item_load();
});

$('body').delegate('#trash_item','click',function(event){
  event.preventDefault();
  var id = $(this).attr('rID');
  $.ajax({
    type:'get',
    url:'/trash/cart/item/'+id,
  })
  flyer_load();
  cart_upper_load();
  cart_item_load();
});


$("body").delegate('#get_add_to_cart','click',function(event) {
  
 event.preventDefault();
  var id = $(this).attr('pid');

  $.ajax({
    type:'get',
    url:'/cart/item/add/btn/'+id,
  })

  cart_item_load();
  cart_upper_load();
  flyer_load();
});



$("body").delegate("#mamabari","click",function(event){
      event.preventDefault();
      function flyToElement(flyer, flyingTo) {
      var $func = $(this);
      var divider = 3;
      var flyerClone = $(flyer).clone();
      $(flyerClone).css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 1000});
      $('body').append($(flyerClone));
      var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
      var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;

      $(flyerClone).animate({
        opacity: 0.4,
        left: gotoX,
        top: gotoY,
        width: $(flyer).width()/divider,
        height: $(flyer).height()/divider
      }, 700,
      function () {
        $(flyingTo).fadeOut('fast', function () {
          $(flyingTo).fadeIn('fast', function () {
            $(flyerClone).fadeOut('fast', function () {
              $(flyerClone).remove();
            });
          });
        });
      });
    }
      var itemImg = $(this).parent().parent().find('img').eq(0);
      flyToElement($(itemImg), $('.cart_anchor'));


      var id = $(this).attr('pid');

      $.ajax({
        type:'get',
        url:'/cart/item/add/'+id,
      })

      flyer_load();
      cart_upper_load()
      cart_item_load();


    });

});
