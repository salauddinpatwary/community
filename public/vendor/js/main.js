
$(document).ready(function () {
    $(".fa-bars").click(function () {
        $(".navigation-items").slideToggle();
    });

    $("#tabBtnFrist").click(function () {
        $("#tabBtnFrist").addClass("active-btn");
        $("#tabBtnFrist").removeClass("btn-tab-normal");
        $("#tabBtnSecond").addClass("btn-tab-normal");
        $("#tabBtnSecond").removeClass("active-btn");
    });
    $("#tabBtnSecond").click(function () {
        $("#tabBtnSecond").addClass("active-btn");
        $("#tabBtnSecond").removeClass("btn-tab-normal");
        $("#tabBtnFrist").addClass("btn-tab-normal");
        $("#tabBtnFrist").removeClass("active-btn");
    });

});