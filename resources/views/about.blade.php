@extends('layouts.app')
@section('title')
About Us
@endsection
@section('content')
<!-- start-post-details-area -->
    <section class="privacy-details py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-sm-12">
                    <div class="card p-3">
                        <div class="card-body">
                            <h3 class="card-title text-center">About Us</h3>
                            <hr>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Card Some quick example</h5>
                            <p class="card-text text-left">Use our powerful mobile?</p>
                            <p class="card-text text-left">Use our powerful mobile-first flexbox grid to build layouts
                                of all shapes and sizes thanks to a twelve column system,
                                five default responsive tiers, Sass variables and mixins, and dozens of predefined
                                classes.</p>
                            <p class="card-text text-left">Use our powerful mobile-first flexbox grid to build layouts
                                of all shapes and sizes thanks to a twelve column system,
                                five default responsive tiers, Sass variables and mixins, and dozens of predefined
                                classes.</p>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Card Some quick </h5>
                            <p class="card-text text-left">Use our powerful mobile-first flexbox grid to build layouts
                                of all shapes and sizes thanks to a twelve column system,
                                five default responsive tiers, Sass variables and mixins, and dozens of predefined
                                classes.</p>

                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Card Some quick example</h5>
                            <p class="card-text text-left">Use our powerful mobile-first flexbox grid to build layouts
                                of all shapes and sizes thanks to a twelve column system,
                                five default responsive tiers, Sass variables and mixins, and dozens of predefined
                                classes.</p>
                            <p class="card-text text-left">Use our powerful mobile-first flexbox grid to build layouts
                                of all shapes and sizes thanks to a twelve column system,
                                five default responsive tiers, Sass variables and mixins, and dozens of predefined
                                classes.</p>
                            <p class="card-text text-left">Use our powerful mobile-first flexbox grid to build layouts
                                of all shapes and sizes thanks to a twelve column system,
                                five default responsive tiers, Sass variables and mixins, and dozens of predefined
                                classes.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
<!-- end-post-details-area -->
@endsection