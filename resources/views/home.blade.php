@extends('layouts.app')
@section('title')
Community
@endsection
@section('content')

    <!-- search-area -->
    <section class="search-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form action="{{route('search')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <input name="name" type="text" class="form-control" id="" placeholder="Name/ Keyword">
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <input name="location" type="text" class="form-control" id="" placeholder="Location">
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <select name="profession_id" class="form-control">
                                    <option>Select Option</option>
                                    @if($professions->count() > 0)
                                        @foreach($professions as $profession)
                                            <option value="{{$profession->id}}">{{$profession->title}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <input type="submit" value="SEARCH PROFILE" class="btn btn-secondary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- search-area -->

    <!-- start-profile-community-area -->
    <section class="profile-community py-5">
        <div class="container">
            @if($professions->count() > 0)
                @foreach($professions as $profession)
                    <div class="row">
                        <div class="col-12">
                            <h6 class="text-left text-uppercase">{{$profession->title}}</h6>
                        </div>
                        @if($profession->profiles->count() > 0)
                        @php $i = 1; @endphp
                            @foreach($profession->profiles as $alumni)
                                @if($i == 4) @break @endif
                                @if($alumni->isVerified)
                                    <div class="col-md-3 col-sm-6">
                                        <div data-toggle="modal" data-target="#exampleModal" class="card">
                                            <img src="{{asset($alumni->avatar)}}" alt="">
                                            <div class="card-body">
                                                <h5 class="card-title"><a href="{{route('profile.show',['profile' => $alumni->id])}}">{{$alumni->user->name}} | {{$alumni->profession->title}}</a></h5>
                                                <h6 class="card-subtitle mb-2 text-muted">{{$alumni->company}}({{$alumni->designation}})</h6>
                                                <p class="card-text"><i class="fas fa-map-marker-alt mr-1"></i>Lives in {{$alumni->living}} </p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @php ++$i; @endphp
                            @endforeach
                        @else
                            <div class="card">
                                <div class="card-body text-center">
                                    <p>No {{$profession->title}} Found</p>
                                </div>
                            </div>
                        @endif
                        <div class="col-12">
                            <p class="text-right"><a href="{{route('profession.show',['profession' => $profession->id])}}">More <i class="fas fa-caret-right ml-1"></i></a></p>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </section>
    <!-- end-profile-community-area -->
@endsection