<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>we93mgbghs | @yield('title')</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
  rel="stylesheet">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/css/vendors.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/vendors/css/weather-icons/climacons.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/fonts/meteocons/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/vendors/css/charts/morris.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/vendors/css/charts/chartist.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/vendors/css/charts/chartist-plugin-tooltip.css')}}">
  <!-- END VENDOR CSS-->
  <!-- BEGIN MODERN CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/css/app.css')}}">
  <!-- END MODERN CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/css/core/colors/palette-gradient.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/fonts/simple-line-icons/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/css/core/colors/palette-gradient.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/css/pages/timeline.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/css/pages/dashboard-ecommerce.css')}}">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/style.css')}}">
  <!-- END Custom CSS-->
  <link href="{{asset('/css/toastr.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  @yield('styles')

  <!-- Scripts -->
  <script>
      window.Laravel = <?php echo json_encode([
          'csrfToken' => csrf_token(),
      ]); ?>
  </script>
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu" data-col="2-columns">
  <!-- fixed-top-->
  <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item">
            <a class="navbar-brand" href="{{ url('/') }}">
              <h3 class="brand-text brand-logo">
                we93mgbghs
              </h3>
            </a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
            <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
            <li class="nav-item nav-search"><a class="nav-link nav-link-search" href="#"><i class="ficon ft-search"></i></a>
              <div class="search-input">
                <input class="input" type="text" placeholder="Explore Modern...">
              </div>
            </li>
          </ul>
          <ul class="nav navbar-nav float-right">
              <li class="dropdown dropdown-user nav-item">
                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                  <span class="mr-1">Hello,
                    <span class="user-name text-bold-700">
                        {{Auth::user()->name}}
                    </span>
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();"
                    >
                    <i class="ft-power"></i> Logout</a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
              </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li class=" nav-item"><a href="{{route('home')}}"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Home</span></a>
        </li>
        <li class=" nav-item"><a href="{{route('user.list')}}"><i class="la la-user"></i><span class="menu-title" data-i18n="nav.dash.main">User List</span></a>
        </li>
        <li></li>
        <li class=" nav-item"><a><i class="ft ft-align-right"></i><span class="menu-title" data-i18n="nav.cat.main">Notice</a></span>
            <ul class="menu-content">
                <li><a class="menu-item" href="{{route('notice.list')}}" data-i18n="nav.cat.list"><i class="ft ft-list"></i>List</a></li>
                <li><a class="menu-item" href="{{route('notice.create')}}" data-i18n="nav.cat.create"><i class="ft ft-plus"></i>Create</a></li>
            </ul>
        </li>
        <li class=" nav-item"><a><i class="ft ft-briefcase"></i><span class="menu-title" data-i18n="nav.cat.main">Profession</a></span>
            <ul class="menu-content">
                <li><a class="menu-item" href="{{route('profession.index')}}" data-i18n="nav.cat.list"><i class="ft ft-list"></i>List</a></li>
                <li><a class="menu-item" href="{{route('profession.create')}}" data-i18n="nav.cat.create"><i class="ft ft-plus"></i>Create</a></li>
            </ul>
        </li>
      </ul>
    </div>
  </div>
  <div class="app-content container center-layout mt-2">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
            <div class="row">
              <div class="col-2">

              </div>
              <div class="col-10">
                @yield('content')
              </div>
            </div>
      </div>
    </div>
    </div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<footer class="footer footer-static footer-light navbar-border navbar-shadow fixed-bottom">
   <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
     <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2020 <a class="text-bold-800 grey darken-2" href="http://we93mgbghs.org"
       target="_blank">we93mgbghs.org </a>, All rights reserved. </span>
   </p>
 </footer>
 <!-- BEGIN VENDOR JS-->
 <script src="{{asset('/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
 <!-- BEGIN VENDOR JS-->
 <!-- BEGIN PAGE VENDOR JS-->
 <script src="{{asset('/app-assets/vendors/js/charts/chartist.min.js')}}" type="text/javascript"></script>
 <script src="{{asset('/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js')}}"
 type="text/javascript"></script>
 <script src="{{asset('/app-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
 <script src="{{asset('/app-assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>
 <script src="{{asset('/app-assets/vendors/js/timeline/horizontal-timeline.js')}}" type="text/javascript"></script>
 <!-- END PAGE VENDOR JS-->
 <!-- BEGIN MODERN JS-->
 <script src="{{asset('/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
 <script src="{{asset('/app-assets/js/core/app.js')}}" type="text/javascript"></script>
 <script src="{{asset('/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
 <!-- END MODERN JS-->
 <script src="{{asset('/app-assets/vendors/js/editors/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
<script src="{{asset('/app-assets/js/scripts/editors/editor-ckeditor.js')}}" type="text/javascript"></script>
 <!-- BEGIN PAGE LEVEL JS-->
 <script src="{{asset('/app-assets/js/scripts/pages/dashboard-ecommerce.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/toastr.min.js')}}"></script>
<script type="text/javascript">
   @if (Session::has('success'))
     toastr.success("{{Session::get('success')}}")
   @elseif (Session::has('info'))
     toastr.info("{{Session::get('info')}}")
   @elseif (Session::has('error'))
       toastr.error("{{Session::get('error')}}")
   @endif
 </script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">

    $("#myTable").dataTable();

</script>

 @yield('scripts')
</body>
</html>
