<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!--Page Title-->
    <title>we93mgbghs | @yield('title') </title>

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--custom Styles Css-->
     <link rel="stylesheet" href="{{asset('vendor/css/style.css')}}">

    <!--Favicons-->
    <link rel="shortcut icon" href="{{asset('vendor/img/favicon.ico')}}" type="image/x-icon">

    <!--Fontawesome-->
    <script src="https://kit.fontawesome.com/6e48e3de41.js" crossorigin="anonymous"></script>
    <link href="{{asset('/css/toastr.min.css')}}" rel="stylesheet">

</head>

<body>
    <!-- start-nav-area -->
    <nav class="nav-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 logo">
                    <a class="navbar-brand" href="{{url('/')}}">we93mgbghs</a>
                </div>
                <div class="res-nav">MENU<i class="fas fa-bars ml-1"></i></div>
                <div class="col-md-9 col-sm-12 navigation-items">
                    <ul class="nav res-menu float-right">
                        <li class="nav-item">
                            <a class="nav-link" @if(!Auth::check()) data-toggle="modal" data-target="#exampleModal" @endif href="{{url('/')}}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('about')}}">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" @if(!Auth::check()) data-toggle="modal" data-target="#exampleModal" @endif href="{{route('home')}}">Community</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" @if(!Auth::check()) data-toggle="modal" data-target="#exampleModal" @endif href="{{route('notice')}}">Notice</a>
                        </li>
                        @if(Auth::check() && (Auth::user()->completeProfile == 0))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        @endif
                        @if(Auth::check() && Auth::user()->completeProfile)
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <img src="{{asset(Auth::user()->profile->avatar)}}" width="30px" height="30px" style="border-radius: 50%;">
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{route('profile')}}">
                                        Profile
                                    </a>
                                  @if(Auth::user()->isAdmin)
                                    <a class="dropdown-item" href="{{route('admin')}}">Admin</a>
                                  @endif
                                  <hr>
                                  <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                      <i class="fas fa-sign-out-alt"></i> Logout
                                  </a>
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- end-nav-area -->
    @yield('content')

    <!-- start-footer-area -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="privacy.html">Privacy</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="privacy.html">Policy</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Important Link</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-12 social-media">
                    <p class="text-left">Connect on social media</p>
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fab fa-facebook"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fab fa-instagram"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fab fa-youtube"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copy">
            <p>&copy; copyright. <span>we93mgbghs | 2020</span></p>
        </div>
    </footer>
    <!-- end-footer-area -->


    <!--jquery.Js-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

    <!--custom Js-->
    <script src="{{asset('vendor/js/main.js')}}"></script>
    <script src="{{asset('/js/toastr.min.js')}}"></script>
    <script type="text/javascript">
       @if (Session::has('success'))
         toastr.success("{{Session::get('success')}}")
       @elseif (Session::has('info'))
         toastr.info("{{Session::get('info')}}")
       @elseif (Session::has('error'))
           toastr.error("{{Session::get('error')}}")
       @endif
     </script>
</body>

</html>