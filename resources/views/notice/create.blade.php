@extends('layouts.admin')
@section('title')
    Create Notice
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title text-center">
                <b>Create Notice</b><br>
            </div>
            <form class="form" action="{{route('notice.store')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id='title' class="form-control" placeholder="Notice Title" required="">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" id='image' class="form-control" required="">
                </div>
                <div class="form-group">
                    <label for="body">Description</label>
                    <textarea name="body" id="ckeditor" cols="30" rows="15" class="ckeditor" required=""></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Create" class="btn btn-primary text-center">
                </div>
            </form>
        </div>
    </div>
@endsection
