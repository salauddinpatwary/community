@extends('layouts.admin')
@section('title')
    Edit Notice
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title text-center">
                <b>Edit Notice</b><br>
            </div>
            <form class="form" action="{{route('notice.update',['notice' => $notice->id])}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" value="{{$notice->title}}" name="title" id='title' class="form-control" placeholder="Notice Title" required="">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" id='image' class="form-control" >
                </div>
                <div class="form-group">
                    <label for="body">Description</label>
                    <textarea name="body" id="ckeditor" cols="30" rows="15" class="ckeditor" required="">{!!$notice->body!!}</textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Update" class="btn btn-primary text-center">
                </div>
            </form>
        </div>
    </div>
@endsection
