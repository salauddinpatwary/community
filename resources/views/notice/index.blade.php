@extends('layouts.app')
@section('content')
<!-- start-post-community-area -->
    <section class="post-community py-5">
        <div class="container">
            <div class="row">

                @if($notices->count() <= 0)
                @else
                    @foreach($notices as $notice)
                        <div class="col-md-4 col-sm-6">
                        <div class="card">
                            <img src="{{asset($notice->image)}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <a href="{{route('notice.show',['notice' => $notice->id])}}">
                                    <h5 class="card-title">{{$notice->title}}</h5>
                                </a>
                                <p class="card-text text-left">@php
                                $sen = implode(' ', array_slice(explode(' ', $notice->body), 0, 20));
                            @endphp
                            {!!$sen!!}...
                        </p>
                                <h6 class=" text-left">Published: {{$notice->created_at->diffForHumans()}}</h6>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
                

                <div class="col-12">
                    <p class="text-right">{{$notices->links()}}<i class="fas fa-caret-right ml-1"></i></p>
                </div>
            </div>
        </div>
    </section>
<!-- end-post-community-area -->
@endsection