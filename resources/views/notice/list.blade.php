@extends('layouts.admin')

@section('title')
    Notice List
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title text-center">
                Notice List
            </div>
            <table class="table table-strip table-hover" id="myTable">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Image</td>
                        <td>Title</td>
                        <td>Posted By</td>
                        <td>Date</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>

                      @if($notices->count() <= 0)
                          <tr>
                              <td colspan="6">No Notice Found</td>
                          </tr>
                      @else
                          <?php $i = 0; ?>
                          @foreach ($notices as $notice)
                            <tr>
                              <th scope="row">{{++$i}}</th>
                              <td>
                                <img src="{{asset($notice->image)}}" width="100px" alt="">
                              </td>
                              <td>{{$notice->title}}</td>
                              <td>{{$notice->user->name}}</td>
                              <td>
                                  {{$notice->created_at}}
                              </td>
                              <td>
                                <a href="{{route('notice.edit',['notice' => $notice->id])}}">Edit</a> | <a href="{{route('notice.show',['notice' => $notice->id])}}">View</a>
                              </td>
                            </tr>
                          @endforeach
                  @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
