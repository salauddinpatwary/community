@extends('layouts.app')
@section('content')
<!-- start-post-details-area -->
    <section class="post-details py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-sm-12">
                    <div class="card p-3">
                        <img src="{{asset($notice->image)}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h6 class="text-right font-weight-normal">Published: {{$notice->created_at->diffForHumans()}}</h6>
                            <h5 class="card-title">{{$notice->title}}</h5>
                            <p class="card-text text-left">{!!$notice->body!!}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- end-post-details-area -->
@endsection