@extends('layouts.app')
@section('title')
Community
@endsection
@section('content')

    <!-- start-profile-community-area -->
    <section class="profile-community py-5">
        <div class="container"> 
            <div class="row">
                <div class="col-12">
                    <h5 class="text-left text-uppercase">{{$profession->title}}</h5>
                </div>
                @if($profession->profiles->count() > 0)
                    @foreach($profession->profiles as $alumni)
                        @if($alumni->isVerified)
                            <div class="col-md-3 col-sm-6">
                                <div data-toggle="modal" data-target="#exampleModal" class="card">
                                    <img src="{{asset($alumni->avatar)}}" alt="">
                                    <div class="card-body">
                                        <h5 class="card-title"><a href="{{route('profile.show',['profile' => $alumni->id])}}">{{$alumni->user->name}} | {{$alumni->profession->title}}</a></h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{{$alumni->company}}({{$alumni->designation}})</h6>
                                        <p class="card-text"><i class="fas fa-map-marker-alt mr-1"></i>Lives in {{$alumni->living}} </p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @else
                    <div class="card">
                        <div class="card-body text-center">
                            <p>No {{$profession->title}} Found</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <!-- end-profile-community-area -->
@endsection