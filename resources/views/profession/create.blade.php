@extends('layouts.admin')
@section('title')
    Add Profession
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title text-center">
                <b>Add Profession</b><br>
            </div>
            <form class="form" action="{{route('profession.store')}}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id='title' class="form-control" placeholder="Profession Name" required="">
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-primary text-center">
                </div>
            </form>
        </div>
    </div>
@endsection
