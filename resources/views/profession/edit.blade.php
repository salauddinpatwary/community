@extends('layouts.admin')
@section('title')
    Edit Profession
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title text-center">
                <b>Edit Profession</b><br>
            </div>
            <form class="form" action="{{route('profession.update',['profession' => $profession->id])}}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id='title' class="form-control" value="{{$profession->name}}" required="">
                </div>
                <div class="form-group">
                    <input type="submit" value="Update" class="btn btn-primary text-center">
                </div>
            </form>
        </div>
    </div>
@endsection
