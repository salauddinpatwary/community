@extends('layouts.admin')

@section('title')
    Professions List
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title text-center">
                Professions List
            </div>
            <table class="table table-strip table-hover" id="myTable">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Title</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>

                      @if($professions->count() <= 0)
                          <tr>
                              <td colspan="3">No Professions Found</td>
                          </tr>
                      @else
                          <?php $i = 0; ?>
                          @foreach ($professions as $profession)
                            <tr>
                              <th scope="row">{{++$i}}</th>
                              <td>{{$profession->title}}</td>
                              <td>
                                <a href="{{route('profession.edit',['profession' => $profession->id])}}">Edit</a>
                              </td>
                            </tr>
                          @endforeach
                  @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
