@extends('layouts.app')
@section('title')
Submit Profile For Review
@endsection
@section('content')
<!-- start-review-profile-area -->
    <section class="review-profile py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-sm-12">
                    <div class="card p-3">
                        <div class="card-body">
                            <form action="{{route('profile.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-12 uploadfile text-center">
                                        <label for="file-upload" class="custom-file-upload">
                                            <i class="fas fa-camera"></i> <span>Add Profile Picture</span>
                                        </label>
                                        <input required="" name="avatar" id="file-upload" type="file" />
                                    </div>
                                </div>
                                <div class="form-row mt-4">
                                    <div class="form-group col-sm-6">
                                        <label for="fullname">Full Name</label>
                                        <input disabled="1" name="name" value="{{Auth::user()->name}}" type="text" class="form-control" id="fullname" placeholder="Full Name">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="emailaddress">Email Address</label>
                                        <input disabled="1" type="email" name="email" value="{{Auth::user()->email}}" class="form-control" id="emailaddress"
                                            placeholder="Email Address">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-6">
                                        <label for="dateofbirth">Date of Birth</label>
                                        <input name="dob" type="text" class="form-control" placeholder="DD/MM/YYYY" id="sscbatch" required="">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="sscbatch">SSC Batch</label>
                                        <input name="ssc_batch" type="text" class="form-control" id="sscbatch" required="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-6">
                                        <label for="profession">Profession</label>
                                        <select class="form-control" name="profession_id" required="">
                                            <option>Please Select</option>
                                            @if($professions->count() > 0)
                                                @foreach($professions as $profession)
                                                    <option value="{{$profession->id}}">{{$profession->title}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="companyname">Company Name</label>
                                        <input name="company" type="text" class="form-control" id="companyname"
                                            placeholder="Company Name">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-6">
                                        <label for="designation">Designation</label>
                                        <input name="designation" type="text" class="form-control" id="designation" placeholder="Designation">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="livingcountry">Living Country</label>
                                        <input name="living" type="text" class="form-control" id="livingcountry"
                                            placeholder="Living Country" required="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-6">
                                        <label for="contactno">Contact No</label>
                                        <input name="phone" type="text" class="form-control" id="contactno" placeholder="Contact No" required="">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="Facebook Id">Facebook Id</label>
                                        <input name="facebook_url" placeholder="https://facebook.com/your.profile" type="text" class="form-control" id="Facebook Id"
                                            placeholder="Facebook Id">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="aboutyourself">About Yourself</label>
                                    <textarea name="short_desc" class="form-control" id="aboutyourself"
                                        placeholder="Write a Short Description About Yourself..." rows="3" required=""></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="form-group col-12 text-center">
                                        <input type="submit" value="Submit for Review" class="btn btn-secondary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- end-review-profile-area -->
@endsection