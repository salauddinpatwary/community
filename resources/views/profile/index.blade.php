@extends('layouts.app')
@section('title')
User Profile
@endsection
@section('content')
<!-- start-user-profile-area -->
    <section class="user-profile pt-5 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-sm-12">
                @if(Auth::user()->completeProfile)
                    <div class="card">
                        <div class="card-img">
                            <div class="bg-layer">
                                <img src="{{asset($profile->avatar)}}" alt="...">
                            </div>
                        </div>
                        <div class="card-body  px-4 py-2">
                            <p class="card-text text-left">{{$profile->short_desc}}</p>
                        </div>
                        <div class="card-body mx-auto">
                            <ul class="nav text-center">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{$profile->facebook_url}}"><i class="fab fa-facebook"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fab fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card-body text-center">
                            <h1>Please Edit Your Profile To Continue</h1>
                            <a href="{{route('profile.create')}}" class="btn btn-dark">Edit Profile</a>
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </section>
    <!-- end-user-profile-area -->

    <!-- start-user-post-area -->
    @if(Auth::user()->completeProfile)
    <section class="user-post pb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-sm-12">
                    @if(Auth::user()->profile->isVerified && (Auth::user()->id == $profile->user_id))
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="card">
                                <div class="card-body text-center">
                                    <img class="mx-auto my-2" src="{{asset('vendor/images/edit-solid.png')}}" width="40px" alt="">
                                    <h6 class="card-subtitle mb-2 text-muted">Creat a Blog Post</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="card">
                                <div class="card-body text-center">
                                    <img class="mx-auto my-2" src="{{asset('vendor/images/business-time-solid.png')}}" width="40px" alt="">
                                    <h6 class="card-subtitle mb-2 text-muted">Post a New Job</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                        @if(Auth::user()->id == $profile->user_id)
                            <div class="card">
                                <div class="card-body text-center">
                                    <h1>Please Wait For Admin Approval to Post</h1>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- end-user-post-area -->

    <!-- start-user-details-area -->
    <section class="user-details pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">PROFILE DETAILS</h4>
                            <hr>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10 offset-md-2">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Name</td>
                                                <td>{{$profile->user->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Dateof Birth</td>
                                                <td>{{$profile->dob}}</td>
                                            </tr>
                                            <tr>
                                                <td>SSC Batch</td>
                                                <td>{{$profile->ssc_batch}}</td>
                                            </tr>
                                            <tr>
                                                <td>Profession</td>
                                                <td>{{$profile->profession->title}}</td>
                                            </tr>
                                            <tr>
                                                <td>Company</td>
                                                <td>{{$profile->company}}</td>
                                            </tr>
                                            <tr>
                                                <td>Designation</td>
                                                <td>{{$profile->designation}}</td>
                                            </tr>
                                            <tr>
                                                <td>Living</td>
                                                <td>{{$profile->living}}</td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td>{{$profile->user->email}}</td>
                                            </tr>
                                            <tr>
                                                <td>Phone</td>
                                                <td>{{$profile->phone}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->id == $profile->user_id)
                        <div class="card-body text-center">
                            <a href="{{route('profile.edit')}}" class="btn btn-dark">Edit Profile</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- end-user-details-area -->
@endsection