@extends('layouts.app')
@section('title')
Community
@endsection
@section('content')

    <!-- search-area -->
    <section class="search-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form action="{{route('search')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <input name="name" type="text" class="form-control" id="" placeholder="Name/ Keyword">
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <input name="location" type="text" class="form-control" id="" placeholder="Location">
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <select name="profession_id" class="form-control">
                                    <option>Select Option</option>
                                    @if($professions->count() > 0)
                                        @foreach($professions as $profession)
                                            <option value="{{$profession->id}}">{{$profession->title}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <input type="submit" value="SEARCH PROFILE" class="btn btn-secondary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- search-area -->

    <!-- start-profile-community-area -->
    <section class="profile-community py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h6 class="text-left text-uppercase">Alumnis</h6>
                </div>
                @if($isName)
                    <div class="col-12">
                        <h3 class="text-left text-uppercase">Searching By Name</h3>
                    </div>
                    @if($name->count() > 0)
                        @foreach($name as $nam)
                            <div class="col-md-3 col-sm-6">
                                <div data-toggle="modal" data-target="#exampleModal" class="card">
                                    <img src="{{asset($nam->profile->avatar)}}" alt="">
                                    <div class="card-body">
                                        <h5 class="card-title"><a href="{{route('profile.show',['profile' => $nam->profile->id])}}">{{$nam->name}} | {{$nam->profile->profession->title}}</a></h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{{$nam->profile->company}}({{$nam->profile->designation}})</h6>
                                        <p class="card-text"><i class="fas fa-map-marker-alt mr-1"></i>Lives in {{$nam->profile->living}} </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-12">
                            <p class="text-right">{{$name->links()}}<i class="fas fa-caret-right ml-1"></i></p>
                        </div>
                    @else
                        <h5>None Found</h5>
                    @endif
                @endif
                @if($isLocation)
                    <div class="col-12">
                        <h3 class="text-left text-uppercase">Searching By Location</h3>
                    </div>
                    @if($location->count() > 0)
                    @foreach($location as $nam)
                            <div class="col-md-3 col-sm-6">
                                <div data-toggle="modal" data-target="#exampleModal" class="card">
                                    <img src="{{asset($nam->avatar)}}" alt="">
                                    <div class="card-body">
                                        <h5 class="card-title"><a href="{{route('profile.show',['profile' => $nam->id])}}">{{$nam->user->name}} | {{$nam->profession->title}}</a></h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{{$nam->company}}({{$nam->designation}})</h6>
                                        <p class="card-text"><i class="fas fa-map-marker-alt mr-1"></i>Lives in {{$nam->living}} </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-12">
                            <p class="text-right">{{$location->links()}}<i class="fas fa-caret-right ml-1"></i></p>
                        </div>
                    @else
                        <h5>None Found</h5>
                    @endif
                @endif
                @if($isProfession)
                    <div class="col-12">
                        <h3 class="text-left text-uppercase">Searching By Profession</h3>
                    </div>
                    @if($byProfession->count() > 0)
                        @foreach($byProfession as $nam)
                            <div class="col-md-3 col-sm-6">
                                <div data-toggle="modal" data-target="#exampleModal" class="card">
                                    <img src="{{asset($nam->avatar)}}" alt="">
                                    <div class="card-body">
                                        <h5 class="card-title"><a href="{{route('profile.show',['profile' => $nam->id])}}">{{$nam->user->name}} | {{$nam->profession->title}}</a></h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{{$nam->company}}({{$nam->designation}})</h6>
                                        <p class="card-text"><i class="fas fa-map-marker-alt mr-1"></i>Lives in {{$nam->living}} </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-12">
                            <p class="text-right">{{$byProfession->links()}}<i class="fas fa-caret-right ml-1"></i></p>
                        </div>
                    @else
                        <h5>None Found</h5>
                    @endif
                @endif
            </div>
            <!-- architect-area-end -->
        </div>
    </section>
    <!-- end-profile-community-area -->
@endsection