@extends('layouts.admin')

@section('title')
    User List
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title text-center">
                User List
            </div>
            <table class="table table-strip table-hover" id="myTable">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Image</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Phone</td>
                        <td>SSC Batch</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>

                      @if($users->count() <= 0)
                          <tr>
                              <td colspan="7">No User Found</td>
                          </tr>
                      @else
                          <?php $i = 0; ?>
                          @foreach ($users as $user)
                            <tr>
                              <th scope="row">{{++$i}}</th>
                              <td>
                              	<img src="{{asset($user->profile->avatar)}}" width="100px" alt="">
                              </td>
                              <td>{{$user->name}}</td>
                              <td>{{$user->email}}</td>
                              <td>
                                  {{$user->profile->phone}}
                              </td>
                              <td>
                                {{$user->profile->ssc_batch}}
                              </td>
                              <td>
                              	 <a href="{{route('profile.show',['profile' => $user->profile->id])}}">View</a> @if(!$user->isVerified)|<a href="{{route('approve',['profile' => $user->profile->id])}}">Approve</a> @endif | @if($user->isAdmin) <a href="{{route('remove.admin',['profile' => $user->profile->id])}}">Remove Admin</a> @else <a href="{{route('make.admin',['profile' => $user->profile->id])}}">Make Admin</a> @endif
                              </td>
                            </tr>
                          @endforeach
                  @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
