@extends('layouts.app')
@section('title')
Home
@endsection
@section('content')
<!-- start-banner-area -->
<section class="banner-area">
    <div class="main-layer-area">
        <div class="container">
            <div class="row main-row">
                <div class="col-md-8 col-sm-12">
                    <h4>If more than 12 columns are placed <br> within a single row, each group of extra columns
                        will,
                        <br> as one
                        unit, wrap onto a new
                        line.</h4>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="card" style="width: 20rem;">
                        <div class="card-header nav nav-tabs">
                            <!-- Nav tabs -->
                            <button type="button" class="btn btn-dark nav-link active-btn" id="tabBtnFrist" data-toggle="tab"
                                href="#home">Member
                                Login</button>
                            <button type="button" class="btn btn-light nav-link btn-tab-normal" id="tabBtnSecond" data-toggle="tab"
                                href="#menu1">Register Now</button>
                        </div>
                        <div class="tab-content-area  p-2">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="home" class="tab-content tab-pane active"><br>
                                    <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="submit" value="Login" class="btn btn-dark btn-md">
                                        </div>
                                        <div class="col-md-9">
                                            <p>or <span>Register with <a href="{{url('/login/facebook')}}"><i class="fab fa-facebook mr-1"></i></a><a href="{{url('/login/google')}}"><i
                                                        class="fab fa-google"></i></a></span></p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                                </div>
                                <div id="menu1" class="tab-content tab-pane fade"><br>
                                    <form action="{{ route('register') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <input placeholder="Name" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input placeholder="Confirm Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="submit" value="Next" class="btn btn-dark btn-md">
                                                </div>
                                                <div class="col-md-9">
                                                    <p>or <span>Register with <a href="{{url('/login/facebook')}}"><i class="fab fa-facebook mr-1"></i></a><a href="{{url('/login/google')}}"><i
                                                                    class="fab fa-google"></i></a></span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- Tab panes -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end-banner-area -->

<!-- start-new-community-area -->
<section class="new-community py-5">
    <div class="container">
        <div class="row">
            <h3>New in community</h3>
        </div>
        <div class="row">
            @if($newCommers->count() > 0)
                @foreach($newCommers as $newCommer)
                    <div class="col-md-3 col-sm-6">
                        <div data-toggle="modal" data-target="#exampleModal" class="card">
                            <img src="{{asset($newCommer->avatar)}}" alt="">
                            <div class="card-body">
                                <h5 class="card-title">{{$newCommer->user->name}}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">{{$newCommer->company}}({{$newCommer->designation}})</h6>
                                <p class="card-text"><i class="fas fa-map-marker-alt mr-1"></i>Lives in {{$newCommer->living}} </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
    <!-- Modal -->
    <div class="modal index-modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%;">
                        <div class="col-md-10">
                            <button type="button" class="btn btn-dark nav-link d-inline-block active-btn" id="tabBtnFrist" data-toggle="tab"
                                href="#login">Member
                                Login</button>
                            <button type="button" class="btn btn-light nav-link d-inline-block btn-tab-normal" id="tabBtnSecond" data-toggle="tab"
                                href="#registration">Register Now</button>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="login" class="container tab-pane active"><br>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="submit" value="Login" class="btn btn-dark btn-md">
                                        </div>
                                        <div class="col-md-9">
                                            <p>or <span>Register with <a href="{{url('/login/facebook')}}"><i class="fab fa-facebook mr-1"></i></a><a href="{{url('/login/google')}}"><i
                                                        class="fab fa-google"></i></a></span></p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="registration" class="container tab-pane fade"><br>
                            <form action="{{ route('register') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <input placeholder="Name" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input placeholder="Confirm Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="submit" value="Next" class="btn btn-dark btn-md">
                                                </div>
                                                <div class="col-md-9">
                                                    <p>or <span>Register with <a href="{{url('/login/facebook')}}"><i class="fab fa-facebook mr-1"></i></a><a href="{{url('/login/google')}}"><i
                                                                    class="fab fa-google"></i></a></span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                        </div>
                    </div>
                    <!-- Tab panes -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end-new-community-area -->

@endsection