<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Authentication Routes
Auth::routes();
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::group(['middleware' => 'guest'], function(){
	Route::get('/', 'GuestController@welcome')->name('welcome');
});
Route::get('/privacy','GuestController@privacy')->name('privacy');
Route::get('/about','GuestController@about')->name('about');

Route::group(['middleware' => 'auth'],function(){
	Route::get('/user/profile','ProfileController@index')->name('profile');
	Route::get('/user/profile/create','ProfileController@create')->name('profile.create');
	Route::post('/user/profile/store','ProfileController@store')->name('profile.store');
	Route::get('/user/profile/edit','ProfileController@edit')->name('profile.edit');
	Route::post('/user/profile/update','ProfileController@update')->name('profile.update');
});

Route::group(['middleware' => 'verified'],function(){
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/user/profile/show/{profile}','ProfileController@show')->name('profile.show');
	Route::get('/notice','NoticeController@index')->name('notice');
	Route::get('/notice/single/{notice}','NoticeController@show')->name('notice.show');
	Route::post('/search/result','HomeController@search')->name('search');
	Route::get('/community/view/{profession}/profession','ProfessionController@show')->name('profession.show');
});

Route::group(['middleware' => 'admin'], function(){
	Route::get('/admin',function(){
		return view('admin');
	})->name('admin');
	Route::get('/admin/notice/list', 'NoticeController@list')->name('notice.list');
	Route::get('/admin/notice/create','NoticeController@create')->name('notice.create');
	Route::post('/admin/notice/store','NoticeController@store')->name('notice.store');
	Route::get('/admin/notice/edit/{notice}','NoticeController@edit')->name('notice.edit');
	Route::post('/admin/notice/update/{notice}','NoticeController@update')->name('notice.update');
	Route::get('/admin/user/list','ProfileController@list')->name('user.list');
	Route::get('/admin/user/approve/{profile}','ProfileController@approve')->name('approve');
	Route::get('/admin/user/make/admin/{profile}','ProfileController@makeAdmin')->name('make.admin');
	Route::get('/admin/user/remove/admin/{profile}','ProfileController@removeAdmin')->name('remove.admin');
	Route::get('/admin/profession/index','ProfessionController@index')->name('profession.index');
	Route::get('/admin/profession/create','ProfessionController@create')->name('profession.create');
	Route::post('/admin/profession/store','ProfessionController@store')->name('profession.store');
	Route::get('/admin/profession/{profession}/edit','ProfessionController@edit')->name('profession.edit');
	Route::post('/admin/profession/{profession}/update','ProfessionController@update')->name('profession.update');
});

